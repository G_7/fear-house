﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lights : MonoBehaviour
{
    // Start is called before the first frame update
    public Light light1;
    public Light light2;
    public bool isClicked;
    public Animator eye1;
    public Text text;
  
    public void Clicked()
    {
        ByeByeLights();
    }
    private void ByeByeLights() //changes intensity of lights
    {
        isClicked = !isClicked;
        if (isClicked) //if the button is clicked light intensity goes down
        {
            light1.intensity = 0.65f;
            light2.intensity = 0.38f;
            
            text.text = "Turn Light On";
        }
        else
        {
            text.text = "Turn Light Off"; //if the button isn't clicked light intensity stays the same 
            light1.intensity = 1.5f;
            light2.intensity = 1f;
           
        }
        
    }
}
