﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playTime : MonoBehaviour
{
    Animator anim;
    public GameObject doll;
    public bool isClicked;
    // Start is called before the first frame update
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    public void Clicked()
    {
        Crawl();
    }
    private void Crawl() 
    {

        isClicked = !isClicked;
        if (isClicked) //if the button is clicked the animation plays
        {
            anim.SetTrigger("Active"); //Trigger set for when animation is played
            

            
        }
        else
        {
            
            
            anim.SetTrigger("InActive");

            


        }
        
    }
}
