﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MoveAround : MonoBehaviour
{
    public bool isClicked;
    public Image image;
    public Text text;
    public string scene;
    // Start is called before the first frame update
    void Start()
    {
        image.enabled = false;
        text.enabled = false;
    }

    
    public void LoadRoomScene()
    {

        SceneManager.LoadScene(scene); //go back to the room
    }
    public void Clicked()
    {
        HideInfoText();
    }
    private void HideInfoText()
    {
        isClicked = !isClicked;
        if (isClicked)
        {
            image.enabled = true; // if button clicked they are visible
            text.enabled = true;
        }
        else
        {
            image.enabled = false;
            text.enabled = false;

        }
    }
    
}
