﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationMouse : MonoBehaviour
{
    Vector2 mouseLook;
    Vector2 smoothV;
    public float sensitivity = 5.0f;
    public float smooth = 2.0f;

    public float speed = 3.5f;
    private float X;
    private float Y;

    GameObject character;

    // Start is called before the first frame update
    void Start()
    {
        character = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButton(0))
        {
            transform.Rotate(new Vector3(Input.GetAxis("Mouse Y") * speed, -Input.GetAxis("Mouse X") * speed, 0)); // camera rotation follows the mouse in the x and y dirction
            X = transform.rotation.eulerAngles.x; //eulerAngles to overwrite the rotation value each update
            Y = transform.rotation.eulerAngles.y;
          
            transform.rotation = Quaternion.Euler(X, Y, 0); //applying that rotation
           
        }
        
    }
}
