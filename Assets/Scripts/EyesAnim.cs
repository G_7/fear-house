﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyesAnim : MonoBehaviour
{
    // Start is called before the first frame update
    public Light light1;
    public Light light2;
    public GameObject anim , anim1, anim2;
    void Start()
    {
        CanYouSeeMe();


    }
    void CanYouSeeMe() 
    {
        anim.SetActive(false);
        anim1.SetActive(false);
        anim2.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        if (light1.intensity <= 0.65f && light2.intensity <= 0.38f)//stops you from seeing the model before the light is lowered
        {
            anim.SetActive(true);
            anim.GetComponent<Animator>().enabled = true;
            anim.GetComponent<Animator>().Play("Eye2");
            anim1.SetActive(true);
            anim1.GetComponent<Animator>().enabled = true;
            anim1.GetComponent<Animator>().Play("Eye 2");
            
           
            anim2.SetActive(true);
            anim2.GetComponent<Animator>().enabled = true;
            anim2.GetComponent<Animator>().Play("eye 1");
            
        }
        else
        {
            CanYouSeeMe();
            anim.GetComponent<Animator>().enabled = false;
            anim1.GetComponent<Animator>().enabled = false;
            anim2.GetComponent<Animator>().enabled = false;

        }
    }
}
