﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Mouse : MonoBehaviour
{
    

    void Update()
    {
       
        if (Input.GetAxis("Mouse ScrollWheel") > 0) // if scrolling up zoom in
        {
            GetComponent<Camera>().fieldOfView--; //makes field of view smaller
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0) // if scrolling down zoom out
        {
            GetComponent<Camera>().fieldOfView++; //makes field of view larger
        }

    }
}
