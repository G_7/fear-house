﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenDoor : MonoBehaviour
{
    Animator anim;
    public GameObject clown;
    public Text text;
    public bool isClicked;

    private void OnVisible() //Makes the clown model invisible 
    {
        clown.SetActive(false);
    }
    void Start()
    {
        OnVisible();
        anim = gameObject.GetComponent<Animator>();
    }
    public void Clicked()
    {
        Open();
    }
    private void Open() 
    {
        
        isClicked = !isClicked;
        if (isClicked) //if the button is clicked the Door animation plays
        {
            anim.SetTrigger("Active");
            StartCoroutine(ClownAnim());
            text.text = "Close Door";
            
        }
        else
        {
            
            OnVisible();
            anim.SetTrigger("InActive");
            
            
           
            
        }
        
    }
    
    IEnumerator ClownAnim()
    {
        

        //yield on a new YieldInstruction that waits for 1 seconds.
        yield return new WaitForSeconds(1); // So it waits for a 1 seconds before the clown animation plays


        clown.SetActive(true);
    }
}
